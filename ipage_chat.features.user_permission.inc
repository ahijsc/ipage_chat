<?php
/**
 * @file
 * ipage_chat.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ipage_chat_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access drupalchat'.
  $permissions['access drupalchat'] = array(
    'name' => 'access drupalchat',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'chatter' => 'chatter',
      'editor' => 'editor',
    ),
    'module' => 'drupalchat',
  );

  // Exported permission: 'access drupalchat all logs'.
  $permissions['access drupalchat all logs'] = array(
    'name' => 'access drupalchat all logs',
    'roles' => array(
      'administrator' => 'administrator',
      'chatter' => 'chatter',
    ),
    'module' => 'drupalchat',
  );

  // Exported permission: 'access drupalchat own logs'.
  $permissions['access drupalchat own logs'] = array(
    'name' => 'access drupalchat own logs',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
      'chatter' => 'chatter',
      'editor' => 'editor',
    ),
    'module' => 'drupalchat',
  );

  // Exported permission: 'administer drupalchat'.
  $permissions['administer drupalchat'] = array(
    'name' => 'administer drupalchat',
    'roles' => array(
      'administrator' => 'administrator',
      'chatter' => 'chatter',
    ),
    'module' => 'drupalchat',
  );

  // Exported permission: 'administer masquerade'.
  $permissions['administer masquerade'] = array(
    'name' => 'administer masquerade',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'masquerade',
  );

  // Exported permission: 'masquerade as admin'.
  $permissions['masquerade as admin'] = array(
    'name' => 'masquerade as admin',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'masquerade',
  );

  // Exported permission: 'masquerade as user'.
  $permissions['masquerade as user'] = array(
    'name' => 'masquerade as user',
    'roles' => array(
      'administrator' => 'administrator',
      'chatter' => 'chatter',
    ),
    'module' => 'masquerade',
  );

  return $permissions;
}
