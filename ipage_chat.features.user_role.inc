<?php
/**
 * @file
 * ipage_chat.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function ipage_chat_user_default_roles() {
  $roles = array();

  // Exported role: chatter.
  $roles['chatter'] = array(
    'name' => 'chatter',
    'weight' => 3,
  );

  return $roles;
}
